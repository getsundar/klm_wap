let valuesArray = [0, 1, 2, 3, 4, 5];

//task -3 - Total of an array
let getSum = (total, num) => total + num;

//task -4 - Get the even numbers of an array
let checkEven = (value) => (value % 2 == 0 && value != 0) ? value % 2 == 0 : false;

//task -5 - Adding five to all elements
let addValueFive = (value) => value + 5;

//task -6 - Sum of two values
let sumTwoValues = (a) => {
    addedValues = (b) => a + b;
    return addedValues;
};

// Task 7 - Numeric operation
class Number {
    constructor(initialValue) {
        this.val = initialValue;
    }

    sum(value) {
        this.val += value;
        return this;
    }

    minus(value) {
        this.val -= value;
        return this;
    }
    expo(value) {
        this.val *= value;
        return this;
    }
    display() {
        return this.val;
    }
}


//Task 9 - tquery - ES5
function tQuery(arrayString) {
    this.arrayString = arrayString;
    this.convertedArray = [];
    this.reverse = function() {
        for (var i = 0; i < this.convertedArray.length; i++) {
            this.convertedArray[i] = this.convertedArray[i].split("").reverse().join("");
        }
        this.arrayString = this.convertedArray.toString();
        return this;
    }
    this.split = function(delimiter) {
        this.convertedArray = this.arrayString.split(delimiter);
        this.arrayString = this.convertedArray.toString();
        if (this.arrayString.indexOf(",") != -1) {
            this.convertedArray = this.arrayString.split(',');
            this.arrayString = this.convertedArray.toString();
        }
        return this;
    }
    this.join = function(delimiter) {
        this.convertedArray = this.convertedArray.join(delimiter);
        return this;
    }
    this.get = function() {
        return this.convertedArray;
    }
}


(function() {
    document.getElementById("arrayToConsider").innerHTML = "Array: " + valuesArray;
    document.getElementById("task3").innerHTML = "The Sum of array elements:  " + valuesArray.reduce(getSum);
    document.getElementById("task4").innerHTML = "The even elements:  " + valuesArray.filter(checkEven);
    document.getElementById("task5").innerHTML = "Adding 5:  " + valuesArray.map(addValueFive);
    document.getElementById("task6").innerHTML = "Sum(3)(2):  " + sumTwoValues(3)(2);
    document.getElementById("task7").innerHTML = "The result of numberic operation is:  " + new Number(5).sum(7).minus(2).expo(2).display();
    document.getElementById("task9").innerHTML = "tquery class:  [" + new tQuery("12+23|45+67").split("|").reverse().split("+").join("-").get() + "]";
})();
